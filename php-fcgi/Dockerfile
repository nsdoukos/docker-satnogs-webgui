FROM debian:bullseye-slim

ARG WEBROOT
ARG SATNOGS_WEBGUI_GIT_URL

# Dependencties to be installed
ARG DEPENDENCIES="php7.4-cgi network-manager git soapysdr-tools"

# Install dependencies
RUN apt-get update -qq \
    && apt-get install -qq -y $DEPENDENCIES \
    && rm -rf /var/lib/apt/lists/*

# Create necessary directories
RUN mkdir -p $WEBROOT \
    && mkdir /etc/raspap/ \
    && chown www-data:www-data /etc/raspap/ \
    && chmod 764 /etc/raspap/

# Give permissions to www-data user to read journals
RUN usermod -a -G adm www-data

# Install webpage content from git repository
WORKDIR $WEBROOT
RUN rm -rf $WEBROOT/* \
    && export GIT_TAG=$(git ls-remote -q --tags --refs --exit-code --sort=-"v:refname" "$SATNOGS_WEBGUI_GIT_URL"\
	 | awk 'NR==1{split($2,a,"/");print a[3]}') \
    && git clone --depth=1 --branch=$GIT_TAG $SATNOGS_WEBGUI_GIT_URL $WEBROOT

# Copy php files to correct directories
RUN cp $WEBROOT/config/config.php $WEBROOT/includes

# Remove unecessary dependencies and clean apt cache
RUN apt-get purge -qq -y git \
    && apt-get autoremove -qq -y \
    && apt-get autoclean -qq -y

# Change default session save path
RUN echo "session.save_path = \"/tmp\"" | tee -a /etc/php/7.4/cgi/php.ini

ENTRYPOINT ["/bin/sh", "-c", "exec php-cgi -b ${WEBGUI_PHP_FCGI_PORT}"]
