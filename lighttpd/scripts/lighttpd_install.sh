#!/bin/bash

DEPENDENCIES=("lighttpd"\
        "git")

# Install dependencies
function install_deps() {
    apt-get update -qq
    for pkg in ${DEPENDENCIES[@]}; do
        printf "Installing $pkg...\n"
        apt-get -qq -o=Dpkg::Use-Pty=0 install $pkg 
        if [[ $? = 0 ]]; then
            printf "$pkg [\033[1;32mSuccess\033[0m]\n"
        else
            printf "$pkg [\033[0;31mFail\033[0m]\n"
        fi
    done
    
    rm -rf /var/lib/apt/lists/*
}

# Create necessary directories
function create_dirs() {
    # Webserver php files dir
    mkdir -p $WEBROOT
    # Configuration files dir
    mkdir /etc/raspap
    mkdir /etc/raspap/networking
    touch /etc/raspap/raspi_satnogs.conf
}

# Copy necessary configuration files to correct directories
function copy_config() {
    # Site password
    cp $WEBROOT/raspap.php /etc/raspap
    # PHP defines
    cp $WEBROOT/config/config.php $WEBROOT/includes
    # Wireless channels array
    cp $WEBROOT/config/defaults.json /etc/raspap/networking
}

# Download webpage files to server webroot
function install_content() {
    rm -rf /var/www/html/*
    git clone --depth=1 https://gitlab.com/librespacefoundation/satnogs/satnogs-webgui $WEBROOT
}

# Remove git after installed content and clean apt cache
function delete_unnecessary_deps() {
    apt-get purge git
    apt-get autoremove
    apt-get autoclean
}

# Enables fastcgi-php
function enable_fastcgi() {
    lighttpd-enable-mod fastcgi-php    
    service lighttpd force-reload
}

# Enable httpd mods
function enable_httpd_mods() {
    # Enable mod rewrite
    cp /tmp/50-raspap-router.conf /etc/lighttpd/conf-available/
    ln -s /etc/lighttpd/conf-available/50-raspap-router.conf /etc/lighttpd/conf-enabled/50-raspap-router.conf
    # Enable remote fastcgi
    cp /tmp/15-fastcgi-php.conf /etc/lighttpd/conf-available
}

function install() {
    # install dependencies
    install_deps
    # Create directories
    create_dirs
    # Install content
    install_content
    # Move configs to correct path
    copy_config
    # Delete unnecessary dependencies
    delete_unnecessary_deps
    # enable fastcgi
    enable_fastcgi
    # Enable mod_rewrite
    enable_httpd_mods
}

install
